# Tests Unitaires sous JS

UTIlisation de la librairie Jasmine

Commande

Add Jasmine to your package.json

npm install --save-dev jasmine-browser-runner jasmine-core
Initialize Jasmine in your project

npx jasmine-browser-runner init
Set jasmine as your test script in your package.json

"scripts": { "test": "jasmine-browser-runner runSpecs" }
Run your tests

npm test

Pour comprendre comment cela fonctionne, se réfererau fichier de configuration dans /spec/support

On y trouve les répertoires spec

Pour fair eun fichier de test l'ajouter dans /spec et le nommer nom + Spec.js exemple nomSpec.js