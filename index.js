import * as animation from './animation.js';

var lettreProposes = [];
var nbEchec = 0;
var maxEchec = 6;
let motATrouver;
let tabLettre;
let compteurEchec;
var dessin;
var btnVerifier;
const messageDesagreable = "!! Gros Looser !!"

var proposition = document.getElementById('proposition');
var histoSaisie = document.getElementsByClassName('histo-saisie')[0];
var indice = document.getElementById('indice');
compteurEchec = document.getElementById('nb-echec');

let btnRecommencer = document.getElementById('recommencer');
btnRecommencer.addEventListener('click',function(){
    dessin.effacer();
    recommencer();
})

document.addEventListener('keydown', (e) => {
    if(e.key === 'Enter'){
        verifier();
    }
});


appelAPIMot();

function victoire(){
    alert("gagne");
}

function defaite(){
    alerte("perdu");
}

function recommencer(){
    const lettres = document.getElementsByClassName('saisielettre');
    for (let lettre of lettres){
        lettre.innerHTML = ".";
    }
    appelAPIMot();
}

function init(){
    dessin = new animation.Anim();
    btnVerifier = document.getElementById('verifier');
    btnVerifier.addEventListener('click',function(){
        verifier();
    });


    tabLettre = [];
    lettreProposes = [];
    nbEchec = 0;

    indice.innerText = motATrouver.categorie;
    motATrouver.name =  motATrouver.name.replace(new RegExp(/[èéêë]/g),"e");
    motATrouver.name =  motATrouver.name.replace(new RegExp(/[à,â]/g),"a");
    motATrouver.name =  motATrouver.name.replace(new RegExp(/[û,ù]/g),"u");
    motATrouver.name =  motATrouver.name.replace(new RegExp(/[ç]/g),"c");
    motATrouver.name =  motATrouver.name.replace(new RegExp(/[ï,î]/g),"i");
    tabLettre = motATrouver.name.toUpperCase().split('');
    histoSaisie.innerHTML = " ";
    compteurEchec.innerText = "";
    document.getElementsByClassName('message-desagreable')[0].innerText = '';
    
    var ennonce  = `Mot en ${tabLettre.length} lettres`;
    document.getElementsByClassName('ennonce')[0].innerText = ennonce;
    
    const template = document.getElementsByClassName('template-caractere')[0];
    var node = document.importNode(template,true);
    
    var bloclettre = document.getElementsByClassName('lettres')[0];
    var templateDestination = document.getElementsByClassName('saisielettre');
    for (let i = templateDestination.length; i !== 0 ; i-- ){
        bloclettre.removeChild(templateDestination[i-1]);
        bloclettre = document.getElementsByClassName('lettres')[0];
    }
    var templateDestination = document.getElementsByClassName('lettres');
    for (let i = 0; i <  tabLettre.length; i++ ){
        node.innerText = ".";
        node.classList.add("saisielettre");
        templateDestination[0].appendChild(node);
        node = node.cloneNode()
    }


}

function appelAPIMot(){
    let appel = fetch("https://trouve-mot.fr/api/random")
    .then(response =>response.json())
    .then(data => {
        motATrouver = data[0];
        init()
    })

    //Pour le debug
    // motATrouver = {
    //     name : "maéèê",
    //     categorie : "ma"
    // }
    // init();
}

function verifier(){
    if(!proposition.checkValidity()){return};
    let lettre = proposition.value.toUpperCase();
    proposition.value = "";
    if(lettreProposes.indexOf(lettre) !== -1){return};

    lettreProposes.push(lettre);
    histoSaisie.innerHTML = `Vos anciennes propositions : ${lettreProposes.join("-")}`;
    if(tabLettre.indexOf(lettre) === -1){
        nbEchec ++;
        compteurEchec.innerText = `Déjà ${nbEchec} échec `;
        if(nbEchec <7){
            var fonction1 = dessin['animation' + nbEchec]();
        }
        if (nbEchec === maxEchec){
            document.getElementsByClassName('message-desagreable')[0].innerText = messageDesagreable;
            document.removeEventListener('click',btnVerifier)
            alert("perdu, c'était " + motATrouver.name);
        }
        return
    }
    let position = document.getElementsByClassName('saisielettre');
    for (let i = 0; i <  tabLettre.length; i++ ){
      if(tabLettre[i] === lettre){
        position[i].innerHTML = lettre
      }
    }
    let ok = true;
    for (let i = 0; i <  position.length; i++ ){
    if(position[i].innerHTML === "."){
        ok = false
    }
    }
    if(ok){
        victoire();
    }
}