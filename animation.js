class Anim {

    constructor(){
        this.canvas = document.getElementById('canvas');
        this.ctx = canvas.getContext('2d');
        this.canvas.setAttribute('width', 400);
        this.canvas.setAttribute('height', 500);
        this.dessinDeBase();
    }
    
    dessinDeBase(){
        this.ctx.fillStyle = '#BABABA';
        this.ctx.fillRect(0,480,400,20);
        this.ctx.fillStyle = '#BABA9E';
    }
    
    animation1(){
        this.ctx.fillRect(200,360,20,120);
        this.ctx.fillRect(340,360,20,120);
    }
    animation2(){
        this.ctx.fillRect(40,460,40,20);
        this.ctx.fillRect(60,440,40,20);
        this.ctx.fillRect(80,420,40,20);
        this.ctx.fillRect(100,400,40,20);
        this.ctx.fillRect(120,380,40,20);
        this.ctx.fillRect(150,360,240,20);
    }
    animation3(){
        this.ctx.fillRect(240,140,20,240);
    }
    animation4(){
        this.ctx.fillRect(220,140,140,20);
    }
    animation5(){
        this.ctx.beginPath();
        this.ctx.moveTo(310,150);
        this.ctx.lineTo(310,230);
        this.ctx.closePath();
        this.ctx.stroke(); 
    }
    animation6(){
        this.ctx.beginPath();
        this.ctx.arc(310,240,10,0,2*Math.PI,false);
        this.ctx.stroke();
        this.ctx.closePath();
    }

    effacer(){
        // this.ctx.delete('all');
        this.ctx.clearRect(0,0,400,500);
    }
}

export {Anim};
